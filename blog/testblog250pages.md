---
slug: seminarunterlagen
title: Seminarunterlagen der NSI-Schulung verfügbar
authors:
  - name: Florian Wenzel
    title: Angehender Verwaltungsinformatiker
    url: https://www.strassenbau.niedersachsen.de/startseite/
    image_url: '/img/autor-wenzel.jpg'
tags: [Schulungen]
---

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

