const fs = require('fs');
const path = require('path');

const maxPages = 200;
const maxFolder = 10;

// Funktion zum Erstellen einer einzelnen Seite
function createPage(pageData, index, foldername) {
  const content = `---
sidebar_position: ${index}
---

# ${pageData.heading}

${pageData.content}
`;
  const filePath = path.join(__dirname, foldername, `subtutorial-${index}.md`);
  fs.writeFileSync(filePath, content);
}

// Hauptfunktion zum Durchlaufen aller Datensätze
function generatePages() {
  for(let j = 1; j < maxFolder + 1; j++) {
    let foldername = "tutorial-" + j;
    fs.mkdirSync(path.join(__dirname, foldername));
    for(let i = 1; i < maxPages + 1; i++) {

      let pageData = {};
      pageData.title = "Seite " + i + " von Tutorial " + j;
      pageData.heading = "Tolle Überschrift für Seite " + i + " von Tutorial " + j;
      pageData.content = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

      createPage(pageData, i, foldername);
    }
  }
}

generatePages();
